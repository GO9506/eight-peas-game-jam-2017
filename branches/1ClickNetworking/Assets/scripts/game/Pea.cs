﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Pea :  NetworkBehaviour{

    public GameObject mProjectilePrefab;
	void Start ()
    {
		
	}
	

	void Update ()
    {
        if (!isLocalPlayer)
            return;

	    if(Input.GetKeyDown(KeyCode.Space))
        {
            CmdFireProjectile(transform.position,transform.rotation);
        }	
	}

    [Command]
    void CmdFireProjectile(Vector3 pPosition, Quaternion pRotation)
    {
        GameObject fProj = (GameObject)Instantiate(mProjectilePrefab, pPosition, pRotation);
        
        Rigidbody fRbody = fProj.GetComponent<Rigidbody>();
        fRbody.velocity = fProj.transform.forward * 10;

        NetworkServer.Spawn(fProj);
        Destroy(fProj, 3f);
    }
}
