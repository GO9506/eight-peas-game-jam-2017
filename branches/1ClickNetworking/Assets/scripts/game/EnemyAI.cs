﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.AI;
public class EnemyAI : NetworkBehaviour {

    // Use this for initialization
    GameObject mTargetPlayer;
    NavMeshAgent mAgent;

	void Start ()
    {
        mAgent = GetComponent<NavMeshAgent>();
        mTargetPlayer = GameObject.FindGameObjectWithTag("Player");

        InvokeRepeating("SetPosition", 0f, 1f);
    }

    void SetPosition()
    {
        mAgent.SetDestination(mTargetPlayer.transform.position);
    }

    void OnDestroy()
    {
        CancelInvoke("SetPosition");
    }
    public override void OnNetworkDestroy()
    {

        base.OnNetworkDestroy();
    }
}
