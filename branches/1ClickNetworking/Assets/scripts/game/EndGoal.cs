﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGoal : MonoBehaviour {


    void OnTriggerEnter(Collider pOther)
    {
        if(pOther.tag == "Player")
        {
            GameManager.instance.WinGame();
        }
    }
}
