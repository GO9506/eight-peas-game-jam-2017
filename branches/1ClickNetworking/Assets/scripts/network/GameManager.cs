﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.VR;
using UnityEngine.UI;

public class GameManager : NetworkManager {

    public static GameManager instance;

    //Structs and Enums
    public enum GameState {PREGAME, WAIT, GAME, RESTART};

    //Application Related Variables
    public GameState mGameState;
    private bool mIsServer = false;
    private bool mIsMatchMaking = false;

    GameObject[] mStartpositions;//start positoins are used for spw
    public GameObject mNonVRPlayer;
    public int mPlayersInMatch = 0;
    bool mhost = true;
    List<int> mPlayerIDs = new List<int>();
    
    int mCoroutineBreak = 10;//temporary lmiit to how many times to search for a match

    //Game Variables
    GameObject mMainPlayer;
    public float mGameTimer = 0f;
    public int mEnemiesKilled = 0;

    public GameObject mMainPanel;
    public GameObject mScorePanel;
    public Text mConditionText;
    public Text mEnemiesDefeatedText;

    //*****Section GamePlay Manager Function/Methods
    void Awake()
    {
        if( instance == null)
            instance = this;
        else
            Destroy(gameObject);

        //Add a hook into the SceneManager to monitor loaded levels
        SceneManager.sceneLoaded += LoadedLevel;

    }

    // Use this for initialization
    void Start () {

        //Start the gameState in Pregame until otherwise changed
        mGameState = GameState.PREGAME;
    }


    void Update()
    {
        if(mIsServer && mGameState == GameState.GAME)
        {
            mGameTimer += Time.deltaTime;
        }

        if(Input.GetKeyDown(KeyCode.H) && !mIsMatchMaking)
        {
            mhost = true;
            BeginMatchMaking();
        }
        else if (Input.GetKeyDown(KeyCode.J) && !mIsMatchMaking)
        {
            mhost = false;
            BeginMatchMaking();
        }

        if (!mIsServer)
            return;
        if (Input.GetKeyDown(KeyCode.K) && mGameState == GameState.GAME)
        {
            RestartGame();
        }
        if (Input.GetKeyDown(KeyCode.S) && mGameState == GameState.WAIT)
        {
            mGameState = GameState.GAME;
            BeginGame();
        }
    }

    //What to do when a level changes in the GameManager    
    void LoadedLevel(Scene pScene, LoadSceneMode pMode)
    {
        if (mIsServer)
            if (pScene.name == "game")
            {
                //BeginGame();
                mStartpositions = GameObject.FindGameObjectsWithTag("StartPosition");
            }
            else if (pScene.name == "network")
            {
                //TODO:: check for any necessary cleanup?
                //RetartGame();
            }
    }

    public void BeginMatchMaking()
    {
        if (mIsMatchMaking)
            return;
        GameObject fButt = GameObject.Find("Button");
        fButt.GetComponent<Button>().interactable = false;
        StartMatchMaker();
        
        //if(VRDevice.isPresent)
        if(mhost)
        {
            //if using vr, then start hosting
            mIsServer = true;
            matchMaker.CreateMatch("Match",(uint)maxConnections,true,"","","",0,0,OnMatchCreate);
        }
        else
        {
            //if not then start searching for a game
            mIsMatchMaking = true;
            StartCoroutine(SearchMatch());
        }
    }
    public void MatchList(bool pSuccess, string pExtendedInfo, List<MatchInfoSnapshot> pResponse)
    {
 
        for (int i = 0; i < pResponse.Count; ++i)
        {
            if (pResponse[i].currentSize < maxConnections)
            {
                matchMaker.JoinMatch(pResponse[i].networkId, "", "", "", 0, 0, OnMatchJoined);
                mIsMatchMaking = false;
                break;
            }
        }
    }
    IEnumerator SearchMatch()
    {
        int t = 0;
        while(mIsMatchMaking)
        {
            if(mIsMatchMaking)
                matchMaker.ListMatches(0, 10, "", false, 0, 0, MatchList);

            yield return new WaitForSeconds(6f);
            t++;
            if (t == mCoroutineBreak)
            {
                mIsMatchMaking = false;
                GameObject fButt = GameObject.Find("Button");
                fButt.GetComponent<Button>().interactable = true;
                yield return null;
            }
        }
        yield return null;
    }

    void BeginGame()
    {
        //If we are the server then this will run and start the GameManager's inherent logic
        mGameState = GameState.GAME;
        MoveTowards.mInstance.SetDestination();
        if(AiSpawner.mInstance != null)
            AiSpawner.mInstance.Spawn();
    }
    public void WinGame()
    {
        StartCoroutine(DisplayResults("Victory"));
    }
    public void LooseGame()
    {
        StartCoroutine(DisplayResults("Defeat"));
    }

    IEnumerator DisplayResults(string pMessage)
    {
        AiSpawner.mInstance.Reset();
        mScorePanel.SetActive(true);
        mEnemiesDefeatedText.text = "Enemies Defeated: " + mEnemiesKilled;
        mConditionText.text = pMessage;
        yield return new WaitForSeconds(10f);
        mScorePanel.SetActive(false);
        mEnemiesDefeatedText.text = "Enemies Defeated: ###";
        RestartGame();
    }
    void RestartGame()
    {
        MoveTowards.mInstance.mReady = false;
        mGameState = GameState.WAIT;
        mEnemiesKilled = 0;
        mGameTimer = 0;
        GameObject fStart = GameObject.Find("VRStartPosition");
        mMainPlayer.transform.position = fStart.transform.position;
        mMainPlayer.transform.rotation = fStart.transform.rotation;
        //reset player position and progress.
        //wait until everyone is ready
        //call BeginGame() when starting again
    }

    //*****Section Networking Manager Functions and Methods
    public override void OnStartServer()
    {
        //This is happening before the Game Scene even finishes loading.
        //Set the Server Side Variable here
        mIsServer = true;

        base.OnStartServer();
    }

    public override void OnStopServer()
    {
        //Unset the server side variable if it has been set.
        mIsServer = false;

        //Server Cleanup

        base.OnStopServer();
    }

    //This happens when the game starts and the first person joins.
    public override void OnServerAddPlayer( NetworkConnection pConn, short pPlayerControllerId )
    {
        if (mPlayerIDs.Contains(pConn.connectionId))
            return;
        else
            mPlayerIDs.Add(pConn.connectionId);

        mPlayersInMatch++;
        if (pConn.connectionId != 0)
        {
            GameObject fNonVRplayer = (GameObject)Instantiate(spawnPrefabs[0]);
            fNonVRplayer.transform.position = mStartpositions[pConn.connectionId].transform.position;
            fNonVRplayer.transform.rotation = mStartpositions[pConn.connectionId].transform.rotation;
            fNonVRplayer.transform.SetParent(mStartpositions[pConn.connectionId].transform);
            NetworkServer.AddPlayerForConnection(pConn, fNonVRplayer, pPlayerControllerId);
        }
        else
        {
            GameObject fPlayer = (GameObject)Instantiate(playerPrefab);
            mMainPlayer = fPlayer;
            GameObject fStart = GameObject.Find("VRStartPosition");
            fPlayer.transform.position = fStart.transform.position;
            fPlayer.transform.rotation = fStart.transform.rotation;
            fPlayer.transform.SetParent(fStart.transform);
            NetworkServer.AddPlayerForConnection(pConn, fPlayer, pPlayerControllerId);
        }
        if(mPlayersInMatch >= 8)
        {
            BeginGame();
        }
        //base.OnServerAddPlayer(pConn, pPlayerControllerId);
    }
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        mPlayersInMatch--;
    }
    //Client callbacks ------------------------------------------------------------------------------
    public override void OnClientConnect(NetworkConnection pConn)
    {
        base.OnClientConnect(pConn);
        //pConn.RegisterHandler(MsgKicked, KickedMessageHandler);
        //mPlayerID = pConn.connectionId;

        if (!NetworkServer.active)
        {
            //only to do on pure client (not self hosting client)
        }
    }

    public override void OnClientDisconnect(NetworkConnection pConn)
    {
        base.OnClientDisconnect(pConn);
        //TODO:: Check to see if all of the clients have disconnected, if so, Restart the game?
        //RestartGame();
    }

    public override void OnClientError(NetworkConnection pConn, int pErrorCode)
    {
        Debug.Log("Cient error : " + (pErrorCode == 6 ? "timeout" : pErrorCode.ToString()));
    }

    public override void OnMatchCreate(bool pSuccess, string pExtendedInfo, MatchInfo pMatchInfo)
    {
        Debug.Log("MatchCreated");
        base.OnMatchCreate(pSuccess, pExtendedInfo, pMatchInfo);
        //currentMatchID = (System.UInt64)matchInfo.networkId;
    }

    public void OnMatchDestroyed( bool pSuccess, string pExtendedInfo )
    {
        StopMatchMaker();
        StopHost();
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        mGameState = GameState.WAIT;
        mMainPanel.SetActive(false);
    }
}
