﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;
using UnityEngine.VR;

public class GameManager : NetworkManager {

    public static GameManager instance;

    //Structs and Enums
    public enum GameState {PREGAME, VOTE, GAME, RESTART};

    //Application Related Variables
    public GameState mGameState;
    private bool mIsServer = false;
    private bool mIsMatchMaking = false;

    public GameObject mNonVRPlayer;
    public int mPlayersInMatch = 0;
    bool mhost = true;
    List<int> mPlayerIDs = new List<int>();
    
    int mCoroutineBreak = 10;



    //*****Section GamePlay Manager Function/Methods
    void Awake()
    {
        if( instance == null)
            instance = this;
        else
            Destroy(gameObject);


        //Add a hook into the SceneManager to monitor loaded levels
        SceneManager.sceneLoaded += LoadedLevel;

    }

    // Use this for initialization
    void Start () {


        //Start the gameState in Pregame until otherwise changed
        mGameState = GameState.PREGAME;
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.H) && !mIsMatchMaking)
        {
            mhost = true;
            BeginMatchMaking();
        }
        else if (Input.GetKeyDown(KeyCode.J) && !mIsMatchMaking)
        {
            mhost = false;
            BeginMatchMaking();
        }
    }

    //What to do when a level changes in the GameManager    
    void LoadedLevel(Scene pScene, LoadSceneMode pMode)
    {
        if( mIsServer )
        if( pScene.name == "game" )
            BeginPreGame();
        else if( pScene.name == "network" )
        {
            //TODO:: check for any necessary cleanup?
            //RetartGame();
        }
    }

    public void BeginMatchMaking()
    {
        if (mIsMatchMaking)
            return;
        StartMatchMaker();
        
        //if(VRDevice.isPresent)
        if(mhost)
        {
            //if using vr, then start hosting
            mIsServer = true;
            matchMaker.CreateMatch("Match",(uint)maxConnections,true,"","","",0,0,OnMatchCreate);
            Debug.Log("Creating match");
        }
        else
        {
            //if not then start searching for a game
            mIsMatchMaking = true;
            StartCoroutine(SearchMatch());
            Debug.Log("Searching Match");
        }
    }

    public void MatchList(bool pSuccess, string pExtendedInfo, List<MatchInfoSnapshot> pResponse)
    {
 
        for (int i = 0; i < pResponse.Count; ++i)
        {
            if (pResponse[i].currentSize < maxConnections)
            {
                matchMaker.JoinMatch(pResponse[i].networkId, "", "", "", 0, 0, OnMatchJoined);
                mIsMatchMaking = false;
                break;
            }
        }
    }
    IEnumerator SearchMatch()
    {
        int t = 0;
        while(mIsMatchMaking)
        {
            yield return new WaitForSeconds (6f);
            matchMaker.ListMatches(0, 10, "", false, 0, 0, MatchList);
            t++;
            if (t == mCoroutineBreak)
            {
                mIsMatchMaking = false;
                StopCoroutine(SearchMatch());
            }
        }
        yield return null;
    }

    void BeginPreGame()
    {
        //If we are the server then this will run and start the GameManager's inherent logic

    }


    //*****Section Networking Manager Functions and Methods
    public override void OnStartServer()
    {
        //This is happening before the Game Scene even finishes loading.
        //Set the Server Side Variable here
        mIsServer = true;

        base.OnStartServer();
    }

    public override void OnStopServer()
    {
        //Unset the server side variable if it has been set.
        mIsServer = false;

        //Server Cleanup

        base.OnStopServer();
    }

    //This happens when the game starts and the first person joins.
    public override void OnServerAddPlayer( NetworkConnection pConn, short pPlayerControllerId )
    {
        if (mPlayerIDs.Contains(pConn.connectionId))
            return;
        else
            mPlayerIDs.Add(pConn.connectionId);
        mPlayersInMatch++;
        if (pConn.connectionId != 0)
        {
            GameObject fNonVRplayer = (GameObject)Instantiate(spawnPrefabs[0], Vector3.zero, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(pConn, fNonVRplayer, pPlayerControllerId);
        }
        else
        {
            GameObject fPlayer = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
            NetworkServer.AddPlayerForConnection(pConn, fPlayer, pPlayerControllerId);
        }
        //base.OnServerAddPlayer(pConn, pPlayerControllerId);
    }
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        mPlayersInMatch--;
    }

    //Client callbacks ------------------------------------------------------------------------------
    public override void OnClientConnect(NetworkConnection pConn)
    {
        base.OnClientConnect(pConn);
        //pConn.RegisterHandler(MsgKicked, KickedMessageHandler);
        //mPlayerID = pConn.connectionId;

        if (!NetworkServer.active)
        {
            //only to do on pure client (not self hosting client)
        }
    }

    public override void OnClientDisconnect(NetworkConnection pConn)
    {
        base.OnClientDisconnect(pConn);
        //TODO:: Check to see if all of the clients have disconnected, if so, Restart the game?
        //RestartGame();
    }

    public override void OnClientError(NetworkConnection pConn, int pErrorCode)
    {
        Debug.Log("Cient error : " + (pErrorCode == 6 ? "timeout" : pErrorCode.ToString()));
    }

    public override void OnMatchCreate(bool pSuccess, string pExtendedInfo, MatchInfo pMatchInfo)
    {
        base.OnMatchCreate(pSuccess, pExtendedInfo, pMatchInfo);
        //currentMatchID = (System.UInt64)matchInfo.networkId;
    }

    public void OnMatchDestroyed( bool pSuccess, string pExtendedInfo )
    {
        StopMatchMaker();
        StopHost();
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        mGameState = GameState.GAME;
    }
}
