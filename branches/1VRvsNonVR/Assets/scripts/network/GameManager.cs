﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

public class GameManager : NetworkManager {

    public static GameManager instance;

    //Structs and Enums
    public enum GameState {PREGAME, VOTE, GAME, RESTART};

    //Application Related Variables
    public GameState mGameState;
    private bool mIsServer = false;


    //*****Section GamePlay Manager Function/Methods
    void Awake()
    {
        if( instance == null)
            instance = this;
        else
            Destroy(gameObject);


        //Add a hook into the SceneManager to monitor loaded levels
        SceneManager.sceneLoaded += LoadedLevel;

    }

    // Use this for initialization
    void Start () {

        Debug.Log( "Displays connected: " + Display.displays.Length );

        //Start the gameState in Pregame until otherwise changed
        mGameState = GameState.PREGAME;
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    //What to do when a level changes in the GameManager    
    void LoadedLevel(Scene pScene, LoadSceneMode pMode)
    {
        if( mIsServer )
        if( pScene.name == "game" )
            BeginPreGame();
        else if( pScene.name == "network" )
        {
            //TODO:: check for any necessary cleanup?
            //RetartGame();
        }
    }

    void BeginPreGame()
    {
        //If we are the server then this will run and start the GameManager's inherent logic

    }


    //*****Section Networking Manager Functions and Methods
    public override void OnStartServer()
    {
        //This is happening before the Game Scene even finishes loading.
        //Set the Server Side Variable here
        mIsServer = true;

        base.OnStartServer();
    }

    public override void OnStopServer()
    {
        //Unset the server side variable if it has been set.
        mIsServer = false;

        //Server Cleanup

        base.OnStopServer();
    }

    //This happens when the game starts and the first person joins.
    public override void OnServerAddPlayer( NetworkConnection pConn, short pPlayerControllerId )
    {
        //Reassign what game object represents the player.
        GameObject fPlayer = (GameObject)Instantiate( playerPrefab, Vector3.zero, Quaternion.identity );

        NetworkServer.AddPlayerForConnection( pConn, fPlayer, pPlayerControllerId );
    }

    //Client callbacks ------------------------------------------------------------------------------
    public override void OnClientConnect(NetworkConnection pConn)
    {
        base.OnClientConnect(pConn);
        //pConn.RegisterHandler(MsgKicked, KickedMessageHandler);
        //mPlayerID = pConn.connectionId;

        if (!NetworkServer.active)
        {
            //only to do on pure client (not self hosting client)
        }
    }

    public override void OnClientDisconnect(NetworkConnection pConn)
    {
        base.OnClientDisconnect(pConn);
        //TODO:: Check to see if all of the clients have disconnected, if so, Restart the game?
        //RestartGame();
    }

    public override void OnClientError(NetworkConnection pConn, int pErrorCode)
    {
        Debug.Log("Cient error : " + (pErrorCode == 6 ? "timeout" : pErrorCode.ToString()));
    }

    public override void OnMatchCreate(bool pSuccess, string pExtendedInfo, MatchInfo pMatchInfo)
    {
        base.OnMatchCreate(pSuccess, pExtendedInfo, pMatchInfo);
        //currentMatchID = (System.UInt64)matchInfo.networkId;
    }

    public void OnMatchDestroyed( bool pSuccess, string pExtendedInfo )
    {
        StopMatchMaker();
        StopHost();
    }

}
