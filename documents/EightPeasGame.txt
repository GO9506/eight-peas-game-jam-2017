A simple escape the area game

Network Multiplayer

VR Player has the ability to move forward and place down planks to get closer to the finish line

Chipping In, (8 bit tunes)
VRiends (Players in VR and out of VR can play together)
Crowd Control (8 or more players)
Spaced (You can only use the space bar)

Theme Waves

Player will place down boards to navigate the terrain?
Player will automatically move forward at a constant rate.
AI will reuse the boards from behind so the other 7 players have to defend the VR player that is working his way forward?

Player connects but cannot start until the other seven (7) players connects

Game Design Requirements:
+Project creation and svn hosting services
Create the music and the sound effects
+One Click Network Hosting and Client Joining.
+GameManager logic (wait for 8 players, start game) (bool, 2 players for testing)
+StartGame -> setting the VR player at the start position of the level
+RestartGame -> resetting the VR Player to the start position of the level; 
+kills the AI and resets the wave spawn timer
Connecting all of the Non-VR players to the body of the VR Player but facing behind him
Configuring the AI (Melee, Instant Death for the VR Player)
+Spawning the AI
VR Movement controls (Via Touch Controllers, Wand Controllers, or Xbox One GameController)
+NonVR controls space bar -> shoots out a projectile that kills enemy melee AI
+Win condition when passing the win obstical
VR player can spawn boards to connect the platforms together for navigation
Time + enemy kills = score
