﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpawnPlank : NetworkBehaviour {

    public GameObject mCamera;
    public GameObject mPlankPrefab;

	// Use this for initialization

    public override void OnStartLocalPlayer()
    {
        mCamera.SetActive(true);
        base.OnStartLocalPlayer();
    }
    // Update is called once per frame
    void Update ()
    {
	    if(Input.GetKeyDown(KeyCode.Space))
        {
            CmdSpawnPlank();
        }	
	}

    [Command]
    void CmdSpawnPlank()
    {
        Quaternion rot = new Quaternion(0, transform.rotation.y, 0,0);
        GameObject fPlank = (GameObject)Instantiate(mPlankPrefab,transform.forward * 1.5f,rot);

        NetworkServer.Spawn(fPlank);
    }
}
