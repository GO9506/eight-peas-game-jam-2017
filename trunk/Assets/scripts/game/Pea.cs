﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Pea :  NetworkBehaviour{

    public GameObject mProjectilePrefab;
    public GameObject mCamera;
    public float mAtkTime = 3f;

    float t = 0;
    public override void OnStartLocalPlayer()
    {
        mCamera.SetActive(true);
        base.OnStartLocalPlayer();
    }

    void Update ()
    {
        if (!isLocalPlayer)
            return;

	    if(Input.GetKeyDown(KeyCode.Space) &&  t <= 0)
        {
            CmdFireProjectile(transform.position,transform.rotation);
            t = mAtkTime;
        }	
        if(t > 0)
        {
            t -= Time.deltaTime;
        }
	}

    [Command]
    void CmdFireProjectile(Vector3 pPosition, Quaternion pRotation)
    {
        GameObject fProj = (GameObject)Instantiate(mProjectilePrefab, pPosition, pRotation);
        
        Rigidbody fRbody = fProj.GetComponent<Rigidbody>();
        fRbody.velocity = fProj.transform.forward * 10;

        NetworkServer.Spawn(fProj);
        Destroy(fProj, 3f);
    }
}
