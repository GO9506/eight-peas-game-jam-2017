﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTowards : MonoBehaviour {

    public static MoveTowards mInstance;
    public List<Transform> mWaypoints;
    NavMeshAgent mAgent;
    int mIndex = 0;
    public bool mReady = false;
	
    void Start()
    {
        if(mInstance == null)
            mInstance = this;
        mAgent = GetComponent<NavMeshAgent>();
    }

	// Update is called once per frame
	void Update ()
    {
        if (mReady)
        {
            Vector3 fForward = Camera.main.transform.forward;
            fForward.y = 0;
            mAgent.SetDestination(fForward);
            /*
            Vector3 fpos1 = transform.position;
            Vector3 fpos2 = mWaypoints[mIndex].position;
            fpos1.y = 0;
            fpos2.y = 0;
            if(Vector3.Distance(fpos1,fpos2) < 2)
            {
                mIndex++;
                if (mIndex < mWaypoints.Count)
                    SetDestination();
                
            }
        }*/
        }
	}

    public void SetDestination()
    {
        if (!mReady)
            mReady = true;
        mAgent.SetDestination(mWaypoints[mIndex].position);
    }
}
