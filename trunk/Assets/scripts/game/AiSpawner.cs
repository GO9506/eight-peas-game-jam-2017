﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AiSpawner : NetworkBehaviour {

    public static AiSpawner mInstance;
    public GameObject mAiPrefab;
    public Transform mEnemySpawn;
    float mWaveTimer = 6f;
    int mEnemiesPerWave = 10;
    float mTimeBetweenEnemies = 2f;
    List<GameObject> mActiveEnemies = new List<GameObject>();

	void Start ()
    {
        if (mInstance == null)
            mInstance = this;
	}

    [Server]
    public void Reset()
    {
        StopAllCoroutines();
        foreach(GameObject g in mActiveEnemies)
        {
            NetworkServer.Destroy(g);
        }
        mActiveEnemies.Clear();
    }

    [Server]
    public void Spawn()
    {
        StartCoroutine(WaveSpawn());
    }
    [Server]
    IEnumerator WaveSpawn()
    {
        if (mActiveEnemies.Count == 0)//if there are no enemies, then spawn x amount of them
        {
            yield return new WaitForSeconds(mWaveTimer);
            int mcount = mEnemiesPerWave;
            while (mcount > 0)
            {
                yield return new WaitForSeconds(mTimeBetweenEnemies);// wait this many seconds between each enemy spawned
                CmdSpawnEnemy();
                mcount--;
            }
        }
        
        while(mActiveEnemies.Count > 0)//if there are still enemies
        {
            if (mActiveEnemies.Contains(null))
                mActiveEnemies.Remove(null);
            yield return null;
        }
        yield return new WaitForSeconds(mWaveTimer);

        yield return WaveSpawn();

    }
    [Command]
    void CmdSpawnEnemy()
    {
        GameObject fNME = (GameObject)Instantiate(mAiPrefab, mEnemySpawn.position, mEnemySpawn.rotation);
        mActiveEnemies.Add(fNME);
        NetworkServer.Spawn(fNME);
    }
}
